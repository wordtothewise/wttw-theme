<?php
/**
 * Template Name: Tools Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<div class="tools">
	
	<div class="arround">
		
		<div class="height clearfix">
			
			<h1><?php the_title(); ?></h1>

			<div class="tools-height-left">
				
				<h2><a href="<?php the_field('top_box_url'); ?>"><?php the_field('top_box_title'); ?></a></h2>
				<h5><?php the_field('top_box_subtitle'); ?></h5>
				<p><?php the_field('top_box_text'); ?></p>
				<a href="<?php the_field('top_box_url'); ?>"><?php _e('Get started', 'WttW'); ?>  <i class="icon-more"></i></a>

			</div>

			<?php $args = array( 
	   'post_type' => 'slide', 
	   'order' => 'DESC',
	   'orderby' => 'date',
	   'posts_per_page' => -1
			);
   query_posts( $args ); ?> 

   <?php if (have_posts()) : ?>

   <div class="tools-height-right">

   	<ul class="bxslider">

   <?php while (have_posts()) : the_post(); ?>

   <li><?php the_post_thumbnail('slide-feature'); ?></li>

   <?php endwhile; ?>

  		</ul>

  	</div>

			<?php endif; ?>

			<?php wp_reset_query(); ?>

		</div>

		<div class="down clearfix">

				<?php $counter = 1; ?>
				<?php while( has_sub_field('add_tool_box') ): ?>

				<?php if ($counter%2 == 0): ?>

				<div class="tools-down-right">
					
					<a href="<?php the_sub_field('tool_box_url'); ?>"><img src="<?php the_sub_field('tool_box_image') ?>" alt="image" /></a>
					<h2 style="margin-top: 6px;"><a href="<?php the_sub_field('tool_box_url'); ?>"><?php the_sub_field('tool_box_title'); ?></a></h2>
					<h5><?php the_sub_field('tool_box_subtitle'); ?></h5>
					<p><?php the_sub_field('tool_box_text'); ?></p>
					<a href="<?php the_sub_field('tool_box_url'); ?>"><?php _e('Get started', 'WttW'); ?>  <i class="icon-more"></i></a>

				</div>

				<?php else: ?>

				<div class="tools-down-left">
					
					<a href="<?php the_sub_field('tool_box_url'); ?>"><img src="<?php the_sub_field('tool_box_image') ?>" alt="image" /></a>
					<h2 style="margin-top: 6px;"><a href="<?php the_sub_field('tool_box_url'); ?>"><?php the_sub_field('tool_box_title'); ?></a></h2>
					<h5><?php the_sub_field('tool_box_subtitle'); ?></h5>
					<p><?php the_sub_field('tool_box_text'); ?></p>
					<a href="<?php the_sub_field('tool_box_url'); ?>"><?php _e('Get started', 'WttW'); ?>  <i class="icon-more"></i></a>

				</div>
					
				<?php endif ?>
										
				<?php $counter++; ?>
				<?php endwhile; ?>

		</div>

	</div>

</div>


<?php get_footer(); ?>