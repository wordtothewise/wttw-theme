<?php
/**
 * Template Name: Contact Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<?php $bg = get_field('main_background'); ?>

<div class="builder contact clearfix" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
	<div class="builder_insider">

	<div class="center sec">
		
		
	<div class="contact_box">
		<h3>By email</h3>
		<div class="contact_box_border"></div>
		<p>Consulting information and quotes<br />
		<a href="mailto:<?php echo date('Md') ?>@contact.wordtothewise.com"><?php echo date('Md') ?>@contact.wordtothewise.com</a></p>
	</div>
	<div class="contact_box">
		<h3>By phone</h3>
		<div class="contact_box_border"></div>
		<p><a href="tel:+18008239674">+1 800 823-9674</a></p>
	</div>
	<div class="contact_box">
		<h3>By mail</h3>
		<div class="contact_box_border"></div>
		<p>Word to the Wise, LLC<br />
530 Lytton Ave  2nd floor<br />
Palo Alto<br />
CA  94301-1541<br />
USA</p>
	</div>

	</div>

	</div>
</div>

<?php get_footer(); ?>