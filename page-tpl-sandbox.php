<?php
/**
 * Template Name: Sandbox Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<?php $bg = get_field('main_background'); ?>

<div class="sandbox" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>

	<div class="one">
		
		<h1><?php the_title(); ?></h1>

		<?php the_content(); ?>

		<div class="two">

				<?php while( has_sub_field('sandbox_add_info') ): ?>

				<h2><?php the_sub_field('sandbox_title') ?></h2>
				<p><?php the_sub_field('sandbox_text') ?></p>
				<a href="<?php the_sub_field('sandbox_url'); ?>"><?php _e('Learn more', 'WttW'); ?>  <i class="icon-more"></i></a>
									
				<?php endwhile; ?>

		</div>

	</div>



</div>

<?php get_footer(); ?>