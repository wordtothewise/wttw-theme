<?php
/**
 * Template Name: Two Column Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<?php $bg = get_field('main_background'); ?>

<div class="builder clearfix" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
	<div class="builder_insider">
	
	<h2><?php the_title(); ?></h2>

	<div class="center sec">
		
		<?php the_content(); ?>


		<?php $bottomInfo = get_field('bottom_info_text'); ?>

		<?php if(!empty($bottomInfo)) : ?>

		<div class="bottom-info">

		<h4><?php the_field('bottom_info_text'); ?></h4>
		<?php the_field('bottom_info_link'); ?>

                </div>

		<?php endif; ?>


	</div>

	<div class="three">
		
		<?php the_post_thumbnail('full'); ?>
 	
	</div>
	</div>
</div>

<?php get_footer(); ?>