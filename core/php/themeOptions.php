<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


require_once(ABSPATH . 'wp-admin/includes/theme.php');

$themeOptions = AwesomeOptions::GetInstance();

add_action('admin_init', array($themeOptions, 'awesomeAddInit'));
add_action('admin_menu', array($themeOptions, 'awesomeAddAdmin'));
add_action('wp_ajax_nopriv_remove_image', array($themeOptions, "removeImage"));
add_action('wp_ajax_remove_image', array($themeOptions, "removeImage"));

Class AwesomeOptions
{

	private static $instance;
	private $optionsName;
	private $shortName;
	private $options;
	private $templatesList;

	// private constructor function
	// to prevent external instantiation
	private function __construct($optionsName='Theme Options', $shortName='to')
	{
		$this->optionsName = $optionsName;
		$this->shortName = $shortName;

		$this->setTemplatesList();

		$this->options = array(
			//main title, changed after tab switching
			array("name" => __("Main Settings"),
				"type" => "title"),
			//tab 1 - Sample tab with all posibble fields
			array("type" => "tab",
				"id" => $this->shortName . "-tab-1"),
						
                        array("name" => "Header","type" => "section"),
                        array("name" => __("Email"),
                                        "id" => $this->shortName . "_header_email",
                                        "type" => "text"
                                ),
                        array("name" => __("Phone"),
                                        "id" => $this->shortName . "_header_phone",
                                        "type" => "text"
                                ),
                        array("type" => "close-section"),  
                        array("name" => "General","type" => "section"),
                        array("name" => __("Blog Title"),
                                        "id" => $this->shortName . "_blog_title",
                                        "type" => "text"
                                ),
                        array("name" => __("Blog Extra Title"),
                                        "id" => $this->shortName . "_blog_extra_title",
                                        "type" => "text"
                                ),
                        array("name" => __("Single Post Extra Title"),
                                        "id" => $this->shortName . "_single_extra_title",
                                        "type" => "text"
                                ),
                        array("type" => "close-section"),  
                        array("name" => "Footer","type" => "section"),
                               //text field
                                 array("name" => __("client login Button url"),
                                        "id" => $this->shortName . "_footer_button",
                                        "type" => "text"
                                ),
                        array("type" => "close-section"),

			/*//section in tab
			array("name" => "General",
				"type" => "section"),
			//text field
			array("name" => __("Simple empty text field"),
				"id" => $this->shortName . "_empty_text_field",
				"type" => "text",
				"desc" => _("Description always is optional and placed above the field by default.")),
			//text field with value
			array("name" => __("Simple text field with default value"),
				"id" => $this->shortName . "_predefined_text_field",
				"type" => "text",
				"std" => "Default value"),
			//textfield with suffix
			array("name" => __("Simple empty text field with suffix"),
				"id" => $this->shortName . "_empty_text_field_suf",
				"type" => "text",
				"after" => " (px)"),
			//textarea
			array("name" => __("Textarea with default value"),
				"id" => $this->shortName . "_predefined_text_field",
				"type" => "textarea",
				"std" => "Default value"),
			//text field
			array("name" => __("Editor"),
				"id" => $this->shortName . "_editor",
				"type" => "editor"
			),
			array("type" => "close-section"),
			//another section in tab
			array("name" => "Second Section",
				"type" => "section"),
			//On-off
			array("name" => __("On-off option"),
				"id" => $this->shortName . "_on_off",
				"type" => "on-off",
				"std" => "1"), //valid values 0 or 1
			//Upload button with url field
			array("name" => __("Upload button"),
				"desc" => __("You can insert the image through the button."),
				"id" => $this->shortName . "_upload",
				"type" => "file",
				"std" => "",
				"btn-text" => __("Insert Image")),
		
			//Slider option
			array("name" => __("Slider option with default value and min-max"),
				"id" => $this->shortName . "_slider",
				"type" => "slider",
				"std" => "10",
				"min" => "0",
				"max" => "200",
				"after" => "px"),
			//Slider option range between 0 and 1
			array("name" => __("Slider option with default value, min-max and range between 0 and 1"),
				"id" => $this->shortName . "_slider2",
				"type" => "slider",
				"std" => "0.5",
				"min" => "0",
				"max" => "100",
				"scale" => "100",
				"after" => "px"),
			//Simple drop-down list
			array("name" => __("Drop-Down"),
				"id" => $this->shortName . "_drop_down",
				"type" => "select",
				"std" => "1",
				"options" => array("1" => __("Yes (default)"), "0" => __("No"))), //pass element as associative array - key -> option value, value -> option text
			//List of papages
			array("name" => __('Select "Name" Page'),
				"id" => $this->shortName . "_page_id",
				"type" => "pages",
				"std" => "0"),
			//Simple drop-down list with multiple select
			array("name" => __("Multiple Drop-Down"),
				"id" => $this->shortName . "_multiple_drop_down",
				"type" => "select",
				"multiple" => true,
				"options" => array("1" => __("Value 1"), "2" => __("Value 2"), "3" => __("Value 3"), "4" => __("Value 4"))),
			array("type" => "close-section"),*/
			
			
			array("type" => "close-tab"),
		);
	}

	// getInstance method
	public static function getInstance($optionsName='Theme Options', $shortName='to')
	{

		if(!self::$instance)
		{
			self::$instance = new self($optionsName, $shortName);
		}

		return self::$instance;
	}

	private function setTemplatesList()
	{
		$templates = get_page_templates();

		$this->templatesList[0] = __("Choose page template...");
		$iterator = 1;
		foreach($templates as $key => $template)
		{
			$index = preg_replace("/page\-|\.php/", "", $template);
			$this->templatesList[$index] = $key;
			$iterator++;
		}
	}
	private function getListOfPages()
    {
        $pages = get_pages();
        $listOfPages = array("0" => __("Select a Page"));
        
        foreach ($pages as $page) {
           $listOfPages[$page->ID] = $page->post_title;
        }
        
        return $listOfPages;
    }

	function awesomeAddAdmin()
	{

		if($_GET['page'] == basename(__FILE__))
		{

			if('save' == $_REQUEST['form-action'])
			{

				$this->saveOptions();

				header("Location: admin.php?page=themeOptions.php&saved=true");
				die;
			}
			else if('reset' == $_REQUEST['form-action'])
			{

				foreach($options as $value)
				{
					delete_option($value['id']);
				}

				header("Location: admin.php?page=themeOptions.php&reset=true");
				die;
			}
		}
		add_menu_page($this->optionsName, $this->optionsName, 'administrator', basename(__FILE__), array($this, 'awesomeAdmin'));
	}

	function awesomeAddInit()
	{
		wp_enqueue_style("admin", get_bloginfo('template_directory') . "/core/css/admin.css", false, "1.0", "all");
		wp_enqueue_style("iphone-checkboxes", get_bloginfo('template_directory') . "/core/css/iphone-checkboxes.css", false, "1.0", "all");
		wp_enqueue_style("jquery-ui-1.8.16.custom", get_bloginfo('template_directory') . "/core/css/ui-lightness/jquery-ui-1.8.16.custom.css", false, "1.0", "all");
		wp_enqueue_script("iphone-style-checkboxes", get_bloginfo('template_directory') . "/core/js/iphone-style-checkboxes.js");
		wp_enqueue_script("stickyBox", get_bloginfo('template_directory') . "/core/js/stickyBox.js");
		wp_enqueue_script("admin", get_bloginfo('template_directory') . "/core/js/admin.js");
	}

	public function awesomeAdmin()
	{

		$i = 0;

		if($_REQUEST['saved'])
			echo '<div id="message" class="updated fade"><p><strong>' . $this->optionsName . ' settings saved.</strong></p></div>';
		if($_REQUEST['reset'])
			echo '<div id="message" class="updated fade"><p><strong>' . $this->optionsName . ' settings reset.</strong></p></div>';
		?>
		<div class="wrapper">

			<div class="aw-options">
				<form method="post" action="admin.php?page=themeOptions.php" enctype="multipart/form-data">

					<?php
					foreach($this->options as $value)
					{
						switch($value['type'])
						{

							case "close-tab":
								?>
						</div>
						<?php
						break;

					case "open":
						?>
						<div class="<?php echo $value['container-type']; ?>">
							<?php
							break;

						case "close-section":
							?>
							</tbody>
							</table>
							<input name="save" type="submit" value="<?php _e("Save changes"); ?>" class="button-primary save-changes"/>
							<?php
							break;

						case "title":
							?>
							<h3 class="tab-title"><?php echo $value["name"]; ?></h3>
							<?php
							break;

						case "tab-label":
							?>
							<div id="<?php echo $value["id"]; ?>" class="tab-label" title="<?php echo $value["title"]; ?>">
								<?php echo $value["name"]; ?>
							</div>

							<?php
							break;

						case "tab":
							?>
							<div class="<?php echo $value["id"]; ?> tab">
								<?php
								break;

							case "section":
								?>
								<table cellspacing="0" class="widefat">
									<thead>
										<tr><th colspan="2"><?php echo $value["name"]; ?></th></tr>
									</thead>
									<tbody>
										<?php
										break;

									case 'text':
										?>

										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>
												<p><?php echo $value['desc']; ?></p>

												<input class="widefat" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value='<?php
					if(get_settings($value['id']) != "")
					{
						_e(stripslashes(get_settings($value['id'])));
					}
					else
					{
						_e($value['std']);
					}
										?>' /><?php echo $value['after']; ?>
											</td>

										</tr>
										<?php
										break;

									case 'on-off':
										?>

										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>
												<p><?php echo $value['desc']; ?></p>

												<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="checkbox" class="on-off" value="1" <?php
					if(get_option($value['id']) == "1" || (get_option($value['id']) == NULL && $value['std'] == "1"))
					{
						echo 'checked="checked"';
					}
										?>					/>
											</td>
										</tr>

										<?php
										break;

									case 'file':
										?>

										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>
												<div class="upload-area" style="width: 80%;">
													<p><?php echo $value['desc']; ?></p>

													<?php $image = wp_get_attachment_image_src(get_option($value['id']), "full"); ?>
													<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>_txt" type="text" value="<?php echo $image[0]; ?>" /><br/>
													<div class="file-wrapper">
														<a href="#" class="button-secondary"><?php echo $value['btn-text']; ?></a>
														<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" />
													</div>
													<input type="hidden" name="action" value="wp_handle_upload" />
												
												</div>

												<?php $curValue = get_option($value['id']); ?>
												<?php if(!empty($curValue)): ?>
													<div class="image-preview">
														<?php echo wp_get_attachment_image(get_option($value['id']), "thumbnail"); ?>
														<a href="#" id="remove_<?php echo $value['id']; ?>" class="button-secondary remove-img"><?php _e("Remove"); ?></a>

													
												</div>
												<?php endif; ?>
											</td>
										</tr>
										<?php
										break;

									case 'slider':
										?>

										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>

												<?php if(!empty($value['desc'])): ?>
													<p><?php echo $value['desc']; ?></p>
												<?php endif; ?>
												<script>
													$(function() {
														var scale = ($("#<?php echo $value['id']; ?>").attr("title")).split("-");
														$( "#<?php echo $value['id']; ?>_slider" ).slider({
															min: <?php echo $value['min'] ? $value['min'] : '0'; ?>,
															max: <?php echo $value['max'] ? $value['max'] : '300'; ?>,
															value: <?php echo get_settings($value['id']) ? get_settings($value['id']) : $value['std']; ?>*parseInt(scale[1]),
															change: function(event, ui) {
																$("#<?php echo $value['id']; ?>").val(ui.value/parseInt(scale[1]));
															}
														});
													});
												</script>

												<div id="<?php echo $value['id']; ?>_slider"></div>


												<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" title="scale-<?php echo $value['scale'] ? $value['scale'] : '1'; ?>" class="slider-input" type="text" style="width: 50px; text-align: center;" value="<?php
							if(get_settings($value['id']) != "")
							{
								echo stripslashes(get_settings($value['id']));
							}
							else
							{
								echo $value['std'];
							}
												?>" /><?php echo $value['after']; ?>
											</td>

										</tr>
										<?php
										break;

									case 'select':
										?>
										<?php $active = explode(",", get_option($value['id'])); ?>
										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>
												<p><?php echo $value['desc']; ?></p>
												<select name="<?php echo $value['id']; ?>[]" id="<?php echo $value['id']; ?>" <?php if($value['multiple'])
						echo 'multiple="true" size="3"'; ?>>
															<?php
															foreach($value['options'] as $key => $option)
															{

																$selected = (in_array($key, $active)) ? 'selected="selected"' : '';
																echo '<option value="' . $key . '" ' . $selected . '>' . $option . '</option>';
															}
															?>
												</select>

											</td>

										</tr>
										<?php
										break;
case 'pages':
										?>
										<?php $active = explode(",", get_option($value['id'])); ?>
										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>
												<p><?php echo $value['desc']; ?></p>
												<select name="<?php echo $value['id']; ?>[]" id="<?php echo $value['id']; ?>" <?php if($value['multiple'])
						echo 'multiple="true" size="3"'; ?>>
															<?php
															foreach($this->getListOfPages() as $key => $option)
															{

																$selected = (in_array($key, $active)) ? 'selected="selected"' : '';
																echo '<option value="' . $key . '" ' . $selected . '>' . $option . '</option>';
															}
															?>
												</select>

											</td>

										</tr>
										<?php
										break;
									case 'textarea':
										?>

										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>
												<?php if(!empty($value['desc'])): ?>
													<p><?php echo $value['desc']; ?></p>
												<?php endif; ?>
												<textarea class="widefat" cols="1" rows="1" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php
							if(get_settings($value['id']) != "")
							{
								echo (stripslashes(get_option($value['id'])));
							}
							else
							{
								echo $value['std'];
							}
												?></textarea>

											</td>

										</tr>
										<?php
										break;
										?>
										<?php
											case 'editor':
										?>

										<tr>
											<td class="opt-name"><?php echo $value['name']; ?></td>
											<td>
												<?php if(!empty($value['desc'])): ?>
													<p><?php echo $value['desc']; ?></p>
												<?php endif; ?>
													<?php $content = ''; 
													if(get_settings($value['id']) != "")
							{
								$content = stripslashes(get_option($value['id']));
							}
							else
							{
								$content = $value['std'];
							}
													
													?>
													<?php wp_editor($content, $value['id']); ?>
													<br/>
											</td>

										</tr>
										<?php
										break;
										?>

									<?php
								}
							}
							?>


						<input type="hidden" name="action" value="wp_handle_upload" />
						<input type="hidden" name="form-action" value="save" />
						</form>
				
				</div>
			</div>
			<?php
		}

		public function saveOptions()
		{
			foreach($this->options as $value)
			{
				if(!empty($_POST['save']))
				{
					if($value['type'] == "file" && $_FILES[$value['id']]['size'])
					{

						// if it is an image
						if(preg_match('/(jpg|jpeg|png|gif|x-icon|ico)$/', $_FILES[$value['id']]['type']))
						{
							$override = array($value['id'] => true);
							// save the file, and store an array, containing its location in $file
							
							
							$aId = $this->addImage($_FILES[$value['id']]);
			
							if(function_exists("wp_get_attachment_image_src"))
							{
								
							}
							
							//$file = wp_handle_upload($_FILES[$value['id']], $override);

							update_option($value['id'], $aId);
						}
						else
						{
							// Not an image.
							// Die and let the user know that they made a mistake.
							wp_die('No image was uploaded.');
						}
					}
					else if($value['type'] == "on-off")
					{
						if(isset($_REQUEST[$value['id']]))
						{
							update_option($value['id'], 1);
						}
						else
						{
							update_option($value['id'], 0);
						}
					}
					else if(isset($_REQUEST[$value['id']]) && !empty($_REQUEST[$value['id']]) && $value['type'] != "file")
					{
						if(is_array($_REQUEST[$value['id']]))
						{
							update_option($value['id'], implode(",", $_REQUEST[$value['id']]));
						}
						else
						{
							update_option($value['id'], $_REQUEST[$value['id']], "Theme Options");
						}
					}
					else if($value['type'] != "file" && $value['type'] != "hidden")
					{
						delete_option($value['id']);
					}
				}
				else
				{
					if($value['type'] == "file")
					{
						update_option($value['id'], "");
					}
					else if($value['type'] == "on-off")
					{
						if(isset($value['std']) && $value['std'] = 1)
						{
							update_option($value['id'], 1);
						}
						else
						{
							update_option($value['id'], 0);
						}
					}
					else if($value['type'] != "file" && $value['type'] != "hidden")
					{
						if(is_array($value['std']))
						{
							update_option($value['id'], implode(",", $value['std']));
						}
						else
						{
							update_option($value['id'], $value['std']);
						}
					}
				}
			}
		}

		public function getPagesList()
		{
			$pages = get_pages();
			$aPagesList = array();
			$aPagesList[0] = __("Choose page...");

			foreach($pages as $page)
			{
				$aPagesList[$page->ID] = $page->post_title;
			}

			return $aPagesList;
		}

		public function getCategoriesList()
		{
			$categories = get_categories();
			$aCategoriesList = array();

			$aCategoriesList[0] = __("Choose categories...");

			foreach($categories as $category)
			{
				$aCategoriesList[$category->cat_ID] = $category->name;
			}

			return $aCategoriesList;
		}

		public function getTemplatesList()
		{
			return $this->templatesList;
		}

		public function addImage($upload)
		{
			
			$filename = $upload['name'];
			$uploads = wp_upload_dir(); /* Get path of upload dir of wordpress */

			if(is_writable($uploads['path'])) /* Check if upload dir is writable */
			{
				if((!empty($upload['tmp_name']))) /* Check if uploaded image is not empty */
				{
					if($upload['tmp_name']) /* Check if image has been uploaded in temp directory */
					{
						$overrides = array( 'test_form' => false );
						$file = wp_handle_upload($upload, $overrides); /* Call our custom function to ACTUALLY upload the image */

						$attachment = array /* Create attachment for our post */
							(
							'post_mime_type' => $file['type'], /* Type of attachment */
							'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
						);

						$aid = wp_insert_attachment($attachment, $file['file']);  /* Insert post attachment and return the attachment id */

						require_once(ABSPATH . "wp-admin" . '/includes/image.php');
						$attach_data = wp_generate_attachment_metadata( $aid, $file['file'] );
						wp_update_attachment_metadata($aid,  $attach_data);

							
						return $aid;
					}
				}
				else
				{
					echo 'Please upload the image.';
				}
			}
			return NULL;
		}

		public function removeImage()
		{
			$aId = get_option($_POST['image_key']);
			if(delete_option($_POST['image_key']))
			{
				wp_delete_attachment($aId);
				echo "0";
			}
			die();
		}

	}
	?>
