;(function ( $, window, document, undefined ) {


	var pluginName = "stickyBox",
	defaults = {
		side: null,
		offset: 0,
		sideOffset: null,
		topOffset: '0',
		zIndex: null,
		width: null
	};

	function Plugin ( element, options ) {
		this.element = element;
		this.settings = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = pluginName;
		this.init();
	}

	Plugin.prototype = {
		init: function () {
			var that = this;
			var $this = $(this.element);
			var offsetTop = $this.offset().top;
			this.createWrapper();
			$(window).scroll(function(){
				if($(window).scrollTop() >= offsetTop - that.settings.offset ) {
					$this.css({'position':'fixed'});
					if(that.settings.side) $this.css(that.settings.side,that.settings.sideOffset);
					if(that.settings.topOffset) $this.css('top',that.settings.topOffset);
					if(that.settings.zIndex) $this.css('zIndex',that.settings.zIndex);
					if(that.settings.width) $this.css('width',that.settings.width);
				} else {
					$this.removeAttr('style');
				}
			});
			return this;
		},
		createWrapper : function() {
			var $this = $(this.element);
			$this.wrap('<div class="stickybox-placeholder"></div>');
			$this.parent('.stickybox-placeholder').width($this.outerWidth(true)).height($this.outerHeight(true));
			return this;
		}
	};
	
	$.fn[ pluginName ] = function ( options ) {
		return this.each(function() {
			if ( !$.data( this, "plugin_" + pluginName ) ) {
				$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
			}
		});
	};

})( jQuery, window, document );