var $ = jQuery.noConflict();

$(document).ready(
	function(){

		$('.wp-admin #submitdiv').stickyBox({
			offset : 28,
			width : 278,
			zIndex : 99,
			topOffset : 28
		});
		
		$('.on-off:checkbox').iphoneStyle();
		$(".aw-options input[type=file]").css("opacity", "0").mouseenter(
			function()
			{
				$(this).siblings("a").addClass("hover");
			});
		$("input[type=file]").mouseleave(
			function()
			{
				$(this).siblings("a").removeClass("hover");
			});

		$(".slider-input").blur(function()
		{
			var scale = ($(this).attr("title")).split("-");
						
			var newVal = $(this).val()*parseInt(scale[1]);
			$("#"+$(this).attr("id")+"_slider").slider("option", "value", newVal)
		});

		$(".tab-label:first").addClass("active");
		$(".tab").not(":first").slideUp(100);
		
		$(".tab-label").live("click",
			function(e)
			{
				e.preventDefault();
				$(".tabs-container .tab-label").removeClass("active");
				$(this).addClass("active");
				$(".tab").slideUp(500);
				$("."+$(this).attr("id")).slideDown(500);
				$(".tab-title").text($(this).attr("title"));
			});

			$("input[type=file]").change(
			function()
			{
				$(".image-path").remove();
				$(this).parent().siblings("input").not(".image_resize").val("").after("<div class='image-path'>"+$(this).val()+"</div>");
			});

			$(".image-path").live("click",
			function()
			{
				var inputEl = $(this).siblings("input");
				$(this).remove();
				$(inputEl).focus();
			});
			
			$(".remove-img").live("click", function(e)
			{
				var tmp = $(this).attr("id").toString().split("remove_");
				var parentEl = $(this).parent();
				e.preventDefault();
				$.ajax({
					url: ajaxurl,
					type: "POST",
					data: {action: "remove_image", image_key: tmp[1]},
					success: function(data)
					{
						if(data==0)
						{
							$("#"+tmp[1]+"_txt").val("");
							$(parentEl).empty().append("<span style='color: red;'>Image has been removed!</span>");
						}
					}
				});
				
			});
	});
