<?php
//*****************************************************************************/
//
//  1. Init
//	2. Admin UI 
//	3. Navigation
//	4. Widgets
//	5. Thumbnails
//	6. Custom Post Types
//	7. Shortcodes
//	8. Excerpts
//	9. Meta Boxes
//	10. Built-In functions
//	11. Custom Funcitons
//
//*****************************************************************************/


/* * ******************************** Init ************************************* */
include_once (TEMPLATEPATH . "/core/php/init.php");
add_filter('wp_footer', 'localize_scripts');
add_filter('widget_text', 'do_shortcode');
add_post_type_support( 'page', 'excerpt' );
add_theme_support('html5');
add_theme_support('automatic-feed-links');
/* * **************************** END Init ************************************* */

 if( !is_admin()){
 
wp_deregister_script('jquery');
wp_register_script('jquery', "//code.jquery.com/jquery-1.11.0.min.js", false, '1.11.0', true);
wp_enqueue_script('jquery');
}
 


function wttw_tags_and_share() {
	$sl = wp_get_shortlink();
	?><table class="wttwtas"><tr><td><?php
	the_tags('<span class="tags">',' • ','</span>');
	?></td><td align="right"><span class="share bubble"><i class="trigger icon-share-square-o icon-2x"></i>
	<div class="popup"><table>
	<tr>
	<td><a href="https://plus.google.com/share?url=<?php echo urlencode($sl);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="google"><i class="icon-googleplus icon-2x"></i></a></td>
	<td><a href="https://twitter.com/intent/tweet?url=<?php echo urlencode($sl);?>&amp;text=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=420,width=550');return false;" class="twitter"><i class="icon-twitter icon-2x"></i></a></td>
	<td><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($sl);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="facebook"><i class="icon-facebook icon-2x"></i></a></td>
	</tr>
	<tr><td colspan="3"><div class="input-group">
	<input class="form-control" readonly="readonly" value="<?php echo $sl; ?>">
	<span class="input-group-btn">
	<button class="btn btn-default copy-button" data-clipboard-text="<?php echo $sl; ?>" title="Copy to clipboard"><i class="icon-paste"></i></button>
	</span>
	</td></tr>
	</table>
	</div></span></td></tr></table><?php
}

function wttw_comment($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);
  ?>
  <li class="li-comment" id="li-comment-<?php comment_ID() ?>">

    <div id="div-comment-<?php comment_ID() ?>" class="div-comment">
      <div id="comment-<?php comment_ID() ?>" <?php comment_class() ?>> 
        <address class="vcard author entry-title comment-author">
          <span class="photo avatar">
            <?php echo get_avatar($comment, 48 ); ?>
          </span>
          <cite class="fn">
            <?php echo get_comment_author_link() ?>
          </cite>
          <span class="says">says</span>
        </address>
        <div class="entry-content comment-content">
          <?php if ($comment->comment_approved == '0') : ?>
            <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
            <br />
          <?php endif; ?>
          <?php comment_text() ?>
        </div>
        <div class="clear"></div>
        <div class="comment-meta commentmetadata small">
          <span class="date comment-date">
            <abbr class="published" title="<?php comment_time('Y-m-d\TH:i') ?>"><?php comment_date() ?>,
              <a title="<?php _e('Permanent link to this comment') ?>" rel="bookmark" 
                href="<?php the_permalink(); ?>#comment-<?php comment_ID(); ?>"><?php comment_time(); ?></a>
              </abbr>
            </span>
            <?php edit_comment_link(__('Edit', 'WttW'), '<div class="edit-comment edit">', '</div>'); ?>
          </div>
        </div>
      </div>

        <?php
        
      }


/* * ***************************** Amin UI ************************************* */
add_filter('manage_posts_columns', 'posts_columns_id', 5);
add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);
add_filter('manage_pages_columns', 'posts_columns_id', 5);
add_action('manage_pages_custom_column', 'posts_custom_id_columns', 5, 2);
add_filter('manage_media_columns', 'column_id');
add_filter('manage_media_custom_column', 'column_id_row', 10, 2);

function posts_columns_id($defaults) {
        $defaults['wps_post_id'] = __('ID');
        return $defaults;
}

function posts_custom_id_columns($column_name, $id) {
        if ($column_name === 'wps_post_id') {
                echo $id;
        }
}

function column_id($columns) {
        $columns['colID'] = __('ID');
        return $columns;
}

function column_id_row($columnName, $columnID) {
        if ($columnName == 'colID') {
                echo $columnID;
        }
}

/* * ****************************** END Amin UI ******************************** */


/* * ******************************* Navigation ******************************** */
register_nav_menus(
        array(
            'primary' => 'Main Navigation',
            'mobile' => 'Main menu on mobile',
            'footer' => 'Footer Navigation'
        )
);
//Modify the above locations to adapt them to the needs of current theme

/* * ***************************** END Navigation ****************************** */


/* * ********************************* Widgets ********************************* */
add_action('init', 'theme_widgets_init');

function theme_widgets_init() {

        // Area 1
        register_sidebar(array(
            'name' => 'Home Widget Area', //Area name
            'id' => 'home_widget_area', //Area ID, used to load the correct area in sidebar files
            'before_widget' => '<div class="mail">', // HTML tag placed before widget
            'after_widget' => "</div>", // HTML tag placed after widget
            'before_title' => '<h3 class="widget-title">', // HTML tag placed before widget title
            'after_title' => '</h3>', // HTML tag placed after widget title
        ));
        register_sidebar(array(
            'name' => 'Blog Widget Area', //Area name
            'id' => 'blog_widget_area', //Area ID, used to load the correct area in sidebar files
            'before_widget' => '<div id="%1$s" class="widget-container %2$s">', // HTML tag placed before widget
            'after_widget' => "</div>", // HTML tag placed after widget
            'before_title' => '<h3 class="widget-title">', // HTML tag placed before widget title
            'after_title' => '</h3>', // HTML tag placed after widget title
        ));
        //Add as much as is required areas based on Primary Widget Area
}

// Check for static widgets in widget-ready areas
function is_sidebar_active($index) {
        global $wp_registered_sidebars;

        $widgetcolums = wp_get_sidebars_widgets();

        if ($widgetcolums[$index])
                return true;

        return false;
}

// end is_sidebar_active

/* * ****************************** END Widgets ******************************** */


/* * ******************************* Thumbanils ******************************** */
if (function_exists('add_theme_support')) {
        add_theme_support('post-thumbnails');
		
		//Add needed size photos, crop parameter can be true or false
        add_image_size('thumbnail-size', 'width', 'height', $crop);
        add_image_size('home-feature', '343', '324', false);
        add_image_size('right-feature', '227', '9999', false);
        add_image_size('slide-feature', '358', '214', true);
        add_image_size('person-feature', '178', '9999', false);

        
		//Add your sizes to dropdown in media library
		add_filter('image_size_names_choose', 'my_image_sizes');
		function my_image_sizes($sizes) {
				$addsizes = array(
					"thumbnail-size" => __( "Thumbnail Name"),
					"thumbnail-size2" => __( "Thumbnail Name 2"),
						);
				$newsizes = array_merge($sizes, $addsizes);
				return $newsizes;
		}
}

/* * ***************************** END Thumbanils ****************************** */


/* * *************************** Custom Post Types ***************************** */
add_action('init', 'create_post_type');

function create_post_type() {
        /* THIS IS AN EXAMPLE, REMOVE IT OR CHANGE PARAMS AND UNCOMMENT
          $labels = array(
          'name' => _x('Books', 'post type general name', 'WttW-admin'),
          'singular_name' => _x('Book', 'post type singular name', 'WttW-admin'),
          'add_new' => _x('Add New', 'book', 'WttW-admin'),
          'add_new_item' => __('Add New Book', 'WttW-admin'),
          'edit_item' => __('Edit Book', 'WttW-admin'),
          'new_item' => __('New Book', 'WttW-admin'),
          'all_items' => __('All Books', 'WttW-admin'),
          'view_item' => __('View Book', 'WttW-admin'),
          'search_items' => __('Search Books', 'WttW-admin'),
          'not_found' => __('No books found', 'WttW-admin'),
          'not_found_in_trash' => __('No books found in Trash', 'WttW-admin'),
          'parent_item_colon' => '',
          'menu_name' => __('Books', 'WttW-admin')
          );
          $args = array(
          'labels' => $labels,
          'public' => true,
          'publicly_queryable' => true,
          'show_ui' => true,
          'show_in_menu' => true,
          'query_var' => true,
          'rewrite' => array('slug' => _x('book', 'URL slug', 'WttW-admin')),
          'capability_type' => 'post',
          'has_archive' => true,
          'hierarchical' => false,
          'menu_position' => null,
          'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
          );
          register_post_type('book', $args); // IMPORTANT!!! USE singular name for custom post type name eg. book, not books
         */

        /* THIS IS AN EXAMPLE FOR TAXONOMIES
          // Add new taxonomy, make it hierarchical (like categories)
          $labels = array(
          'name' => _x( 'Genres', 'taxonomy general name' ),
          'singular_name' => _x( 'Genre', 'taxonomy singular name' ),
          'search_items' =>  __( 'Search Genres' ),
          'all_items' => __( 'All Genres' ),
          'parent_item' => __( 'Parent Genre' ),
          'parent_item_colon' => __( 'Parent Genre:' ),
          'edit_item' => __( 'Edit Genre' ),
          'update_item' => __( 'Update Genre' ),
          'add_new_item' => __( 'Add New Genre' ),
          'new_item_name' => __( 'New Genre Name' ),
          'menu_name' => __( 'Genre' ),
          );

          register_taxonomy('genre',array('book'), array(
          'hierarchical' => true,
          'labels' => $labels,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array( 'slug' => 'genre' ),
          ));

          // Add new taxonomy, NOT hierarchical (like tags)
          $labels = array(
          'name' => _x( 'Writers', 'taxonomy general name' ),
          'singular_name' => _x( 'Writer', 'taxonomy singular name' ),
          'search_items' =>  __( 'Search Writers' ),
          'popular_items' => __( 'Popular Writers' ),
          'all_items' => __( 'All Writers' ),
          'parent_item' => null,
          'parent_item_colon' => null,
          'edit_item' => __( 'Edit Writer' ),
          'update_item' => __( 'Update Writer' ),
          'add_new_item' => __( 'Add New Writer' ),
          'new_item_name' => __( 'New Writer Name' ),
          'separate_items_with_commas' => __( 'Separate writers with commas' ),
          'add_or_remove_items' => __( 'Add or remove writers' ),
          'choose_from_most_used' => __( 'Choose from the most used writers' ),
          'menu_name' => __( 'Writers' ),
          );

          register_taxonomy('writer','book',array(
          'hierarchical' => false,
          'labels' => $labels,
          'show_ui' => true,
          'update_count_callback' => '_update_post_term_count',
          'query_var' => true,
          'rewrite' => array( 'slug' => 'writer' ),
          ));
         */
        $labels = array(
            'name' => _x('Slideshows', 'post type general name', 'WttW-admin'),
            'singular_name' => _x('Slide', 'post type singular name', 'WttW-admin'),
            'add_new' => _x('Add New', 'Slide', 'WttW-admin'),
            'add_new_item' => __('Add New Slide', 'WttW-admin'),
            'edit_item' => __('Edit Slide', 'WttW-admin'),
            'new_item' => __('New Slide', 'WttW-admin'),
            'all_items' => __('All Slides', 'WttW-admin'),
            'view_item' => __('View Slide', 'WttW-admin'),
            'search_items' => __('Search Slides', 'WttW-admin'),
            'not_found' => __('No Slides found', 'WttW-admin'),
            'not_found_in_trash' => __('No Slides found in Trash', 'WttW-admin'),
            'parent_item_colon' => '',
            'menu_name' => __('Slideshows', 'WttW-admin')
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => _x('slide', 'URL slug', 'WttW-admin')),
            'capability_type' => 'page',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array('title', 'thumbnail','page-attributes')
        );
        register_post_type('slide', $args); // IMPORTANT!!! USE singular name for custom post type name eg. Slide, not Slides
		
		$labels = array(
          'name' => _x( 'Slideshows', 'taxonomy general name' ),
          'singular_name' => _x( 'Slideshow', 'taxonomy singular name' ),
          'search_items' =>  __( 'Search Slideshows' ),
          'all_items' => __( 'All Slideshows' ),
          'parent_item' => __( 'Parent Slideshow' ),
          'parent_item_colon' => __( 'Parent Slideshow:' ),
          'edit_item' => __( 'Edit Slideshow' ),
          'update_item' => __( 'Update Slideshow' ),
          'add_new_item' => __( 'Add New Slideshow' ),
          'new_item_name' => __( 'New Slideshow Name' ),
          'menu_name' => __( 'Slideshow' ),
          );

          register_taxonomy('slideshow',array('book'), array(
          'hierarchical' => true,
          'labels' => $labels,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array( 'slug' => 'slideshow' ),
          ));

          $labels = array(
            'name' => _x('Team', 'post type general name', 'WttW-admin'),
            'singular_name' => _x('Person', 'post type singular name', 'WttW-admin'),
            'add_new' => _x('Add New', 'Person', 'WttW-admin'),
            'add_new_item' => __('Add New Person', 'WttW-admin'),
            'edit_item' => __('Edit Person', 'WttW-admin'),
            'new_item' => __('New Person', 'WttW-admin'),
            'all_items' => __('All Persons', 'WttW-admin'),
            'view_item' => __('View Person', 'WttW-admin'),
            'search_items' => __('Search Persons', 'WttW-admin'),
            'not_found' => __('No Persons found', 'WttW-admin'),
            'not_found_in_trash' => __('No Persons found in Trash', 'WttW-admin'),
            'parent_item_colon' => '',
            'menu_name' => __('Team', 'WttW-admin')
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => _x('person', 'URL slug', 'WttW-admin')),
            'capability_type' => 'page',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array('title', 'editor', 'thumbnail','page-attributes')
        );
        register_post_type('person', $args); // IMPORTANT!!! USE singular name for custom post type name eg. Slide, not Slides
}

/* * ************************* END Custom Post Types *************************** */


/* * ***************************** Shortcodes ********************************** */

function custom_button($atts) {
        extract(shortcode_atts(array(
                    'href' => '#',
                    'label' => __('Anchor', "WttW"),
                        ), $atts));

        return "<a href='$href' class='custom-btn'>$label</a>";
}

add_shortcode('c_button', 'custom_button');

function button_shortcode($atts,$content = null) {
        
        extract(shortcode_atts(array(
                    'url' => '#',
                    'type' => ''
                        ), $atts));
        $params = array ();
        
        if(!array_key_exists('url', $atts)) {
            
            foreach($atts as $text) {
                $tmp = explode('=', $text);
                $params[$tmp[0]] = preg_replace('/\"|\'/','', $tmp[1]);
            }
            $params['type'] .= ' '.preg_replace('/\"|\'/','', $atts[2]);
            extract(shortcode_atts(
                    array(
                        'url' => '',
                        'type' => ''
                    ), $params));
        }

        return "<a href='$url' class='button $type'>$content</a>";
}

add_shortcode('button', 'button_shortcode');


function clear_func($atts, $content = "") {
        return "<div class='clearfix'></div>";
}

add_shortcode('clear', 'clear_func');

function span_func($atts, $content = "") {
        return "<span>".do_shortcode($content)."</span>";
}

add_shortcode('s', 'span_func');

function tabs_func($atts, $content = null)
{

	extract(shortcode_atts(array(
				'titles' => ''
					), $atts));
	
	$titles = explode(",", $titles);
	$html = '<div class="tabs">';

		$html .= '<ul>';
		$i=1;
		foreach($titles as $title):
			$html .= '<li><a href="#tabs-'.$i.'" rel="tabs-'.$i.'">'.trim($title).'</a></li>';
			$i++;
		endforeach;
				
		$html .= '</ul>';
		$html .= do_shortcode($content);
	$html .= '</div>';
	return $html;
}

add_shortcode('tabs', 'tabs_func');

function tab_func($atts, $content = null)
{
	extract(shortcode_atts(array(
				'id' => ''), $atts));
	
	$html = '<div id="tabs-'.$id.'">';

		$html .= do_shortcode($content);
	
	$html .= '</div>';
	return $html;
}

add_shortcode('tab', 'tab_func');

function video_func($atts, $content = "") {

		extract(shortcode_atts(array(
        	'url' => '#'
        ), $atts));

        return get_video($url);
}

add_shortcode('video', 'video_func');


function map_func($atts, $content = null)
{
	extract(shortcode_atts(array(
				'address' => '',
                                'id' => ''
        ), $atts));
return '<div class="map">
                                                                <!-- /////////////////////////////////////////////////////////////////////// -->
                                                                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                                                                <script type="text/javascript">
                                                                        // icon options
                                                                        var size = new google.maps.Size(46,31);
                                                                        var start = new google.maps.Point(0,0);
                                                                        var attach = new google.maps.Point(14,31);
                                                                        var homeicon = new google.maps.MarkerImage("'.get_bloginfo('template_directory').'/img/map-marker.png", size, start, attach);
                                                                </script>
                                                                
                                                                <div id="map-wrapper'.$id.'"></div>
                                                                <script type="text/javascript">

                                                                        var geocoder = new google.maps.Geocoder();
                                                                        var addr = "'.$address.'";
                                                                        geocoder.geocode({address: addr }, initGeocodeMap);

                                                                        function initGeocodeMap(results, status)
                                                                        {
                                                                                if(status == google.maps.GeocoderStatus.OK)
                                                                                {

                                                                                        var myOptions = {
                                                                                        center: results[0].geometry.location,
                                                                                        zoom: 12,
                                                                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                                                                        mapTypeControl: false,
                                                                                        disableDefaultUI: true
                                                                                        };
                                                                                        var map'.$id.' = new google.maps.Map(document.getElementById("map-wrapper'.$id.'"),
                                                                                        myOptions);


                                                                                        // stworzenie markera

                                                                                        var markerOpt =
                                                                                        {
                                                                                        position: results[0].geometry.location,
                                                                                        map: map'.$id.',
                                                                                        draggable:false,
                                                                                        clickable:false,
                                                                                        icon:homeicon
                                                                                        }
                                                                                        var marker'.$id.' = new google.maps.Marker(markerOpt);
                                                                                }
                                                                                else
                                                                                {
                                                                                        alert("The given address: " +addr + " has not been found.");
                                                                                }
                                                                        }

                                                                </script>

                                                        </div>';

}
add_shortcode('map', 'map_func');


/* THIS IS SAMPLE SHORTCODE FROM CODEX, CUSTOMIZE IT AND UNCOMMENT OR DELETE
  function bartag_func($atts) {
  extract(shortcode_atts(array(
  'foo' => 'no foo',
  'baz' => 'default baz',
  ), $atts));
  return "foo = {$foo}";
  }
  add_shortcode('bartag', 'bartag_func');
 */
/* * *************************** END Shortcodes ******************************** */


/* * ****************************** Excerpts *********************************** */

function custom_excerpt_length($length) {
        return 16;
}

function custom_excerpt_more($more) {
        global $post;
        return '... <a href="' . get_permalink($post->ID) . '" class="more-link">' . __('Read More', 'WttW') . '</a>';
}

/* * **************************** END Excerpts ********************************* */

/* * ***************************** Meta Boxes ********************************** */
add_action('init', 'add_custom_meta_boxes', 999);

function add_custom_meta_boxes() {
        if (is_admin()) {
                /** USE CORRECT PARAMS AND UNCOMMENT 
                  $prefix = 'prefix_';

                  // Posts Meta Boxes

                  $config = array(
                  'id' => 'post_label_fields', // fields group ID
                  'title' => __('Post Label', 'WttW-admin'), //Fields Section Title
                  'pages' => array('post'), //post types
                  'context' => 'normal',
                  'priority' => 'high',
                  'fields' => array(),
                  'local_images' => false,
                  'use_with_theme' => '/wp-content/themes/theme_folder/core/plugins/meta-box-class' //REMEMBER TO CHANGE THEME FOLDER NAME!!!
                  );

                  $my_meta = new AT_Meta_Box($config);

                  $my_meta->addCheckbox($id, $args, $repeater);
                  $my_meta->addCheckboxList($id, $options, $args, $repeater);
                  $my_meta->addCode($id, $args, $repeater);
                  $my_meta->addColor($id, $args, $repeater);
                  $my_meta->addDate($id, $args, $repeater);
                  $my_meta->addField($id, $args, $repeater);
                  $my_meta->addFile($id, $args, $repeater);
                  $my_meta->addHidden($id, $args, $repeater);
                  $my_meta->addImage($id, $args, $repeater);
                  $my_meta->addParagraph($id, $args, $repeater);
                  $my_meta->addPosts($id, $options, $args, $repeater);
                  $my_meta->addRadio($id, $options, $args, $repeater);
                  $my_meta->addRepeaterBlock($id, $args);
                  $my_meta->addSelect($id, $options, $args, $repeater);
                  $my_meta->addTaxonomy($id, $options, $args, $repeater);
                  $my_meta->addText($id, $args, $repeater);
                  $my_meta->addTextarea($id, $args, $repeater);
                  $my_meta->addTime($id, $args, $repeater);
                  $my_meta->addWysiwyg($id, $args, $repeater);

                  $my_meta->Finish();
                 */
        }
}

/* * *************************** END Meta Boxes ******************************** */


/* * ************************ Built_in Functions ******************************* */

/**
 * Extract page name from page template file
 */
function custom_body_class($sClass = null) {
        $classes = get_body_class();
        $sClassesText = '';
        foreach ($classes as $class) {
                $new_class = str_replace('page-template-', '', $class);
                $new_class = str_replace('page-tpl-', '', $new_class);
                $new_class = str_replace('-php', '', $new_class);
                $sClassesText .= ' ' . $new_class;
        }
        return trim($sClassesText) . ' ' . $sClass;
}

/**
 * Cut text to specific number of signs
 * 
 * @param string $sContent
 * @param int $iLengthLimit
 * @return string Cut text
 */
function stripteaser($sContent, $iLengthLimit) {
        $aContent = split(" ", $sContent);
        $iCurrentLength = 0;
        $sResult = '';

        if (strlen($sContent) <= $iLengthLimit) {
                return $sContent;
        }

        for ($i = 0; $i < count($aContent); $i++) {
                $iCurrentLength += (int) (strlen($aContent[$i]));
                if ($iCurrentLength < $iLengthLimit) {
                        $sResult.=$aContent[$i].=" ";
                } else {
                        break;
                }
        }

        return trim($sResult) . "...";
}

/**
 * Remove unnecessary <p> tags
 * @param string $content
 * @return string text without empty paragraphs
 */
function remove_empty_paragraphs($sContent) {
        $sContent = str_replace("]</p>", "]", $sContent);
        $sContent = str_replace("\>\<\/p\>", "\>", $sContent);
        $sContent = str_replace("<p>[", "[", $sContent);
        return $sContent;
}

add_filter('the_content', 'remove_empty_paragraphs');

/**
 * Get Video ID from video URL (Youtube)
 * 
 * @param string $sMovieUrl
 * @return string Movie ID
 */
function get_yt_video_ID($sVideoUrl) {
        $tmp = explode("?v=", $sVideoUrl);
        $signs = preg_split('/(?<!^)(?!$)/u', $tmp[1]);
        $videoID = '';
        for ($i = 0; $i < count($signs); $i++) {
                if (preg_match('/[A-Za-z0-9\-_]+/', $signs[$i])) {
                        $videoID .= $signs[$i];
                }
        }

        return $videoID;
}

/**
 * List attachments from post or page
 * 
 * @param array $args
 * @return array Attachmetns
 */
function list_attached_images($args = null) {
        $argsCustom = $args;

        $args = array(
            'size' => 'full',
            'before_list' => '<ul>',
            'after_list' => '</ul>',
            'before_item' => '<li>',
            'after_item' => '</li>',
            'is_link' => true,
            'link_class' => ''
        );
        $args = array_merge((array) $args, (array) $argsCustom);
		$wp_version = get_bloginfo( 'version' );

		if(version_compare( $wp_version, '3.5', '<' ) )
		{
			if (function_exists('attachments_get_attachments')) {

					echo $args['before_list'];

					$attachments = attachments_get_attachments(get_the_ID());
					$total_attachments = count($attachments);

					if ($total_attachments) :
							for ($i = 0; $i < $total_attachments; $i++) :
									$thumbsrc = wp_get_attachment_image_src($attachments[$i]['id'], $args['size']);
									$imgsrc = wp_get_attachment_image_src($attachments[$i]['id'], 'full');
									?>
									<?php echo $args['before_item']; ?>

									<?php if ($args['is_link']) : ?>
											<a href="<?php echo $imgsrc[0]; ?>" <?php if ($args['link_class']) { ?> class="<?php echo $args['link_class']; ?>" <?php } ?> title="<?php echo $attachments[$i]['title']; ?>">
									<?php endif; ?>
											<img src="<?php echo $thumbsrc[0]; ?>" alt="<?php echo $attachments[$i]['title']; ?>"  />
									<?php if ($args['is_link']) : ?>
											</a>
									<?php endif; ?>
									<?php echo $args['after_item']; ?>

									<?php
							endfor;
					endif;

					echo $args['after_list'];
			}
		}
		else
		{
			 $content = null;
        
			$content .= $args['before_list'];
	 
			$attachments = new Attachments( 'my_attachments' );
		   
			while( $attachments->get() ) :
	 
					$thumbsrc = $attachments->src( $args['size']);
					$imgsrc = $attachments->src('full');
	 
					?>
					<?php $content .= $args['before_item']; ?>
	 
	 
							<?php $content .= '<img src="'.$thumbsrc.'" alt="'.$attachments->field( 'title' ).'"  />'; ?>
	 
					<?php $content .= $args['after_item']; ?>
	 
					<?php
	 
			endwhile;
	 
			$content .= $args['after_list'];
	 
			return $content;
		}
}

function get_lang_list() {
        if (function_exists("icl_get_languages")) {
                $sText = '<ul id="lang">';


                $languages = icl_get_languages('skip_missing=0&orderby=code&dir=asc');

                if (1 < count($languages)) {
                        foreach ($languages as $l) {
                                if ($l['active'] == '1') {
                                        $sText .= '<li class="' . $l['language_code'] . ' active"><a href="' . $l['url'] . '" ><img src="' . get_bloginfo('template_directory') . '/images/flag-' . $l['language_code'] . '.png" alt="" /><span>' . $l['translated_name'] . '</span></a>
					<ul class="sub">
					';
                                }
                        }
                        foreach ($languages as $l) {
                                if ($l['active'] == '0') {
                                        $sText .= '<li class="' . $l['language_code'] . ' "><a href="' . $l['url'] . '"><img src="' . get_bloginfo('template_directory') . '/images/flag-' . $l['language_code'] . '.png" alt="" /><span>' . $l['translated_name'] . '</span></a></li>';
                                }
                        }

                        $sText .= '</ul></li></ul>';

                        echo $sText;
                }
        }
}

function the_video($url, $w = null, $h = null) {
        $videoUrl = $url;


        if (!empty($videoUrl) && preg_match("/youtube/", $videoUrl)) {

                $tmp = explode("?v=", $videoUrl);
                $tmp = explode("&", $tmp[1]);
                $signs = preg_split('/(?<!^)(?!$)/u', $tmp[0]);
                $videoID = '';
                for ($i = 0; $i < count($signs); $i++) {
                        if (preg_match('/[A-Za-z0-9\-_]+/', $signs[$i])) {
                                $videoID .= $signs[$i];
                        }
                }
                $autohide = "?autohide=1";
                $rel = "&amp;rel=0";
                $autoplay = '';
                $opaque = "&amp;wmode=transparent";
                ?>
                <iframe width="<?php echo $w; ?>" height="<?php echo $h; ?>" src="http://www.youtube.com/embed/<?php echo $videoID . $autohide . $rel . $autoplay . $opaque; ?>"></iframe>
                <?php
        }
        if (!empty($videoUrl) && preg_match("/vimeo/", $videoUrl)) {
                if (preg_match("@https@", $videoUrl)) {
                        $videoID = preg_replace('@https:\/\/vimeo.com\/@', '', $videoUrl);
                } else if (preg_match("@http@", $videoUrl)) {
                        $videoID = preg_replace('@http:\/\/vimeo.com\/@', '', $videoUrl);
                }
                ?>
                <iframe src="http://player.vimeo.com/video/<?php echo $videoID; ?>?byline=0" width="<?php echo $w; ?>" height="<?php echo $h; ?>"></iframe>

                <?php
        }
}

function get_video($url, $w = null, $h = null) {
        $videoUrl = $url;


        if (!empty($videoUrl) && preg_match("/youtube/", $videoUrl)) {

                $tmp = explode("?v=", $videoUrl);
                $tmp = explode("&", $tmp[1]);
                $signs = preg_split('/(?<!^)(?!$)/u', $tmp[0]);
                $videoID = '';
                for ($i = 0; $i < count($signs); $i++) {
                        if (preg_match('/[A-Za-z0-9\-_]+/', $signs[$i])) {
                                $videoID .= $signs[$i];
                        }
                }
                $autohide = "?autohide=1";
                $rel = "&amp;rel=0";
                $autoplay = '';
                $opaque = "&amp;wmode=transparent";
                
                return '<iframe  src="http://www.youtube.com/embed/'.$videoID . $autohide . $rel . $autoplay . $opaque.'"></iframe>';
                
        }
        if (!empty($videoUrl) && preg_match("/vimeo/", $videoUrl)) {
                if (preg_match("@https@", $videoUrl)) {
                        $videoID = preg_replace('@https:\/\/vimeo.com\/@', '', $videoUrl);
                } else if (preg_match("@http@", $videoUrl)) {
                        $videoID = preg_replace('@http:\/\/vimeo.com\/@', '', $videoUrl);
                }
                return '<iframe  src="http://player.vimeo.com/video/'.$videoID.'?byline=0" ></iframe>';
                ?>
                <?php
        }
}

function localize_scripts() {
        $params = array(
            "siteUrl" => get_bloginfo('url'),
            "templateUrl" => get_bloginfo('template_directory')
        );
        wp_localize_script('functions', 'SiteVars', $params);
}

/* FILTER POST TYPES IN SEARCH RESULTS */

function my_search_posts_filter($query) {
        if ($query->is_search) {
                $query->set('post_type', array('post', 'page'));
        }
        return $query;
}

//add_filter('pre_get_posts', 'my_search_posts_filter');


function custom_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'WttW' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'custom_wp_title', 10, 2 );


/* * *********************** END Built_in Functions **************************** */

class WttW_Homepage_Feature extends WP_Widget { 

    function __construct() { 
        parent::__construct( 
            'feature', // Base ID 
            'WttW Homepage Feature', // Name 
            array( 'description' => __( 'WttW Homepage Feature Widget', 'text_domain' ), ) // Args 
        ); 
    } 

    public function widget( $args, $instance ) { 
        extract( $args ); 
        $icon = $instance['icon'];
        $desc = $instance['desc'];
        $title = $instance['title'];
        $text = $instance['text'];
        $url = $instance['url'];
        echo $before_widget; 


        if (!empty($icon)):
        								$iconFIle = wp_get_attachment_image($icon);    
                echo $iconFIle; 
            endif; 

            if (!empty($desc)):    
                echo '<span>'.$desc.'</span>'; 
            endif; 
            if (!empty($title)):    
                echo '<h5>'.$title.'</h5>'; 
            endif; 
            if (!empty($text)):    
                echo '<p>'.$text.'</p>'; 
            endif; 
            if (!empty($url)):    
                echo '<a href="'.$url.'">read more <i class="icon-more"></i></a>'; 
            endif; 
        echo $after_widget; 
    } 

				function form($instance)
					{

						$defaults = array('title' => '', "rows-count" => 2);
						$instance = wp_parse_args((array) $instance, $defaults);

						$icon = $instance['icon'];
      $desc = $instance['desc'];
      $title = $instance['title'];
      $text = $instance['text'];
      $url = $instance['url'];
						

						?>
        <p> 
        <label for="<?php echo $this->get_field_name( 'icon' ); ?>"><?php _e( 'Icon id:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" type="text" value="<?php echo esc_attr( $icon ); ?>" /> 
        </p> 
        <p> 
        <label for="<?php echo $this->get_field_name( 'desc' ); ?>"><?php _e( 'Description:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'desc' ); ?>" name="<?php echo $this->get_field_name( 'desc' ); ?>" type="text" value="<?php echo esc_attr( $desc ); ?>" /> 
        </p> 
        <p> 
        <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /> 
        </p> 
        <p> 
        <label for="<?php echo $this->get_field_name( 'text' ); ?>"><?php _e( 'Text:' ); ?></label> 
        <textarea class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" cols="30" rows="10" ><?php echo esc_attr( $text ); ?></textarea> 
        </p>
        <p> 
        <label for="<?php echo $this->get_field_name( 'url' ); ?>"><?php _e( 'Url:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>" /> 
        </p>  
        <?php 
    } 

    /** 
    * WIdget update 
    */ 
    public function update( $new_instance, $old_instance ) { 
        $instance = array(); 
        $instance['icon'] = ( !empty( $new_instance['icon'] ) ) ? strip_tags( $new_instance['icon'] ) : '';         
        $instance['desc'] = ( !empty( $new_instance['desc'] ) ) ? strip_tags( $new_instance['desc'] ) : '';         
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';         
        $instance['text'] = ( !empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';         
        $instance['url'] = ( !empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';         
        return $instance; 
    } 

} 

add_action( 'widgets_init', function(){register_widget( 'WttW_Homepage_Feature' );});

class WttW_Homepage_Blog_Post extends WP_Widget { 

    function __construct() { 
        parent::__construct( 
            'post', // Base ID 
            'WttW Homepage Blog Post', // Name 
            array( 'description' => __( 'WttW Homepage Blog Post Widget', 'text_domain' ), ) // Args 
        ); 
    } 

    public function widget( $args, $instance ) { 
        extract( $args ); 
        $icon = $instance['icon'];
        $desc = $instance['desc'];
        echo $before_widget; 


        if (!empty($icon)):
        								$iconFIle = wp_get_attachment_image($icon);    
                echo $iconFIle; 
            endif; 

            if (!empty($desc)):    
                echo '<span>'.$desc.'</span>'; 
            endif; 
												$args = array( 
            'post_type' => 'post', 
            'order' => 'DESC',
            'orderby' => 'date',
	    'tag' => $desc,	
            'posts_per_page' => 1
        );

        query_posts( $args ); ?> 

        <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

        <h5><?php the_title(); ?></h5>
        <p><?php echo stripteaser(get_the_excerpt(), 170); ?></p>
        <a href="<?php the_permalink(); ?>"><?php _e('read more', 'WttW'); ?> <i class="icon-more"></i></a>

        <?php endwhile; ?>

<?php endif; ?>

<?php wp_reset_query();
        echo $after_widget; 
    } 

				function form($instance)
					{

						$defaults = array('title' => '', "rows-count" => 2);
						$instance = wp_parse_args((array) $instance, $defaults);

						$icon = $instance['icon'];
      $desc = $instance['desc'];
						

						?>
        <p> 
        <label for="<?php echo $this->get_field_name( 'icon' ); ?>"><?php _e( 'Icon id:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" type="text" value="<?php echo esc_attr( $icon ); ?>" /> 
        </p> 
        <p> 
        <label for="<?php echo $this->get_field_name( 'desc' ); ?>"><?php _e( 'Tag:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'desc' ); ?>" name="<?php echo $this->get_field_name( 'desc' ); ?>" type="text" value="<?php echo esc_attr( $desc ); ?>" /> 
        </p> 
        <?php 
    } 

    /** 
    * WIdget update 
    */ 
    public function update( $new_instance, $old_instance ) { 
        $instance = array(); 
        $instance['icon'] = ( !empty( $new_instance['icon'] ) ) ? strip_tags( $new_instance['icon'] ) : '';         
        $instance['desc'] = ( !empty( $new_instance['desc'] ) ) ? strip_tags( $new_instance['desc'] ) : '';         
        return $instance; 
    } 

} 

add_action( 'widgets_init', function(){register_widget( 'WttW_Homepage_Blog_Post' );});

