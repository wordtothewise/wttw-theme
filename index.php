<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */
get_header();
?>
<?php $sHeadText = null; ?>
<?php if(is_archive()) { ?>
        <?php
        if(is_day()) :
                $sHeadText = __('Daily Archives: ', 'WttW') . get_the_date();
        elseif(is_month()) :
                $sHeadText = __('Monthly Archives: ', 'WttW') . get_the_date(_x('F Y', 'monthly archives date format', 'WttW'));
        elseif(is_year()) :
                $sHeadText = __('Yearly Archives: ', 'WttW') . get_the_date(_x('Y', 'yearly archives date format', 'WttW'));
        else :
                $sHeadText = __('Blog Archives ', 'WttW');
        endif;
        ?>
<?php } ?>
<?php if(is_category()) { ?>
        <?php $sHeadText = __("Category: ", "WttW").' '.single_cat_title( '', false ); ?>
<?php } ?>
<?php if(is_author()) { ?>
        <?php $authordata = get_user_by('slug', get_query_var('author_name')); ?>
        <?php $sHeadText = __("Author: ", "WttW").' '.$authordata->display_name; ?>
<?php } ?>
<?php if(is_tag()) { ?>
        <?php $sHeadText = __("Tag: ", "WttW").' '.single_tag_title( '', false ); ?>
<?php } ?>
<?php if(is_search()) { ?>
        <?php $sHeadText = __("Search: ", "WttW").' '.get_search_query(); ?>
<?php } ?>
<?php $bg = get_field('main_background'); ?>

<?php if(is_sidebar_active('blog_widget_area')) : ?>
<div class="builder blog clearfix" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
        <div class="builder_insider clearfix">
        <?php $extrahead = get_option('to_blog_extra_title');
        if ($extrahead) { ?>
              <h3 class="extrahead"><?php echo $extrahead; ?></h3>  
        <?php }
         if ($sHeadText) : ?>
        <h2><?php echo $sHeadText; ?></h2>
        <?php else : ?>
        <h2><?php echo get_option('to_blog_title'); ?></h2>
        <?php endif; ?>

        <div class="center sec">
                
                <?php get_template_part('loop', 'indexmeta'); ?>


        </div>

        <div class="three">
                
                <?php get_sidebar(); ?>
        
        </div>
        </div>
</div>
<?php else : ?>
<div class="open blog" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
        
        <div class="abacus">

                <?php if ($sHeadText) : ?>
                
                <h1><?php echo $sHeadText; ?></h1>

                <?php else : ?>

                <h1><?php echo get_option('to_blog_title'); ?></h1>

                <?php endif; ?>

                <?php get_template_part('loop', 'indexmeta'); ?>

        </div>

</div>
<?php endif; ?>

<?php get_footer(); ?>