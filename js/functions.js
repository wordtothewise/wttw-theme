var $ = jQuery.noConflict();



function submenupaddinit( el ) {

	if(el.length > 0) {
		var moff = el.offset().left;
		var mw = el.width();
		var temp = Math.floor(mw /2) + moff;
		el.children('ul').show();
		var soff = el.find('ul > li:nth-child(1)').offset().left;
		var sw = el.find('ul > li:nth-child(1) .span0').width();
		var calc = temp - (Math.floor(sw /2) + soff);
		el.find('.sub-menu').css('paddingLeft', calc+'px');

	}
			}

function homesubmenupaddinit( el ) {

	if(el.length > 0) {
		var moff = el.offset().left;
		var mw = el.width();
		var temp = Math.floor(mw /2) + moff;
		var submenusel = el.find('.sub-menu');
		submenusel.css('paddingLeft', '');
		/*el.children('a').find('.before').animate(
		    { opacity: 1 },
		    { queue: false, duration: '400' }
		  );*/
		var soff = el.find('ul > li:nth-child(1)').offset().left;
		var sw = el.find('ul > li:nth-child(1) .span0').width();
		var calc = temp - (Math.floor(sw /2) + soff) + 15;
		if (!(submenusel.hasClass('paddinged'))) {
			//submenusel.css('paddingLeft', calc+'px');
			submenusel.css({
				'paddingLeft': calc+'px',
				'display': 'none',
				'opacity': '1'
			});
			submenusel.addClass('paddinged');
		};

	}
			}

function submenupaddresize( el ) {
		el.children('ul').hide();
		el.find('.sub-menu').css('paddingLeft', '0px');
		moff = el.offset().left;
		var mw = el.width();
		var temp = Math.floor(mw /2) + moff;
		el.children('ul').show();
		var soff = el.find('ul > li:nth-child(1)').offset().left;
		var sw = el.find('ul > li:nth-child(1) .span0').width();
		var calc = temp - (Math.floor(sw /2) + soff);
		el.find('.sub-menu').css('paddingLeft', calc+'px');
	}
var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

$(document).ready(function() {
	ZeroClipboard.config( { moviePath: '/wp-content/themes/WttW/ZeroClipboard.swf' } );
	new ZeroClipboard($(".copy-button"));

	$('.bubble').each(function () {
    // options
    var distance = 10;
    var time = 250;
    var hideDelay = 2500;

    var hideDelayTimer = null;

    // tracker
    var beingShown = false;
    var shown = false;
    
    var trigger = $('.trigger', this);
    var popup = $('.popup', this).css('opacity', 0);

    // set the mouseover and mouseout on both element
    $([trigger.get(0), popup.get(0)]).mouseover(function () {
      // stops the hide event if we move from the trigger to the popup element
      if (hideDelayTimer) clearTimeout(hideDelayTimer);

      // don't trigger the animation again if we're being shown, or already visible
      if (beingShown || shown) {
        return;
      } else {
        beingShown = true;

        // reset position of popup box
        popup.css({
          top: -150,
          left: 0,
          display: 'block' // brings the popup back in to view
        })

        // (we're using chaining on the popup) now animate it's opacity and position
        .animate({
          top: '-=' + distance + 'px',
          opacity: 1
        }, time, 'swing', function() {
          // once the animation is complete, set the tracker variables
          beingShown = false;
          shown = true;
        });
      }
    }).mouseout(function () {
      // reset the timer if we get fired again - avoids double animations
      if (hideDelayTimer) clearTimeout(hideDelayTimer);
      
      // store the timer so that it can be cleared in the mouseover if required
      hideDelayTimer = setTimeout(function () {
        hideDelayTimer = null;
        popup.animate({
          top: '-=' + distance + 'px',
          opacity: 0
        }, time, 'swing', function () {
          // once the animate is complete, set the tracker variables
          shown = false;
          // hide the popup entirely after the effect (opacity alone doesn't do the job)
          popup.css('display', 'none');
        });
      }, hideDelay);
    });
  });
	if ($(".single-blog-post").length > 0) {
		$(".blog-page-parent").addClass('blogparent');
	};
	$(".menu > ul > li > .sub-menu").each(function() {
		
		var me = $(this).children('li:first-of-type').children('a');
		var data = me.text();
		me.text('');
		var arr = data.split(' ');

	    $.each(arr,function(index, value){
	        me.append("<span class='span"+index+"'>"+ this + "</span> ");
	    });

	});
	if(!$('body').hasClass('home')){

		submenupaddinit( $('.bluebox > div.menu > ul > li.current-menu-item, .bluebox > div.menu > ul > li.blogparent, .bluebox > div.menu > ul > li.current-menu-parent, .bluebox > div.menu > ul > li.current-page-ancestor, .bluebox > div.menu > ul > li.current_page_ancestor') );
		setTimeout(function(){
			$(".bluebox > div.menu > ul > li > .sub-menu li").animate({'opacity': '1'}, 400);
		}, 110);
	};

	$('.bluebox .menu > ul > li.current-menu-item').addClass('current-menu-parent');

	$('a[href="#"]').click(function(event) {
		event.preventDefault();
	});
	$('.bxslider').bxSlider({
		controls: false,
		adaptiveHeight: true
	});
	
	/*if ($(".bluebox .menu > ul > li:nth-child(1)").length > 0){
		submenupaddinit( $(".bluebox .menu > ul > li:nth-child(1)") );
	}*/
	/*$(".bluebox .menu > ul > li > a").click(function(event) {
		event.preventDefault();
		var me = $(this);
		me.closest("ul").find(".current-menu-parent > .sub-menu").css('padding-left', "");
		me.closest("ul").find(".current-menu-parent").removeClass('current-menu-parent');
		me.parent().addClass('current-menu-parent');
		var mepar = me.parent();
		submenupaddinit( mepar );
	});*/
	
	
	$( '#dl-menu' ).dlmenu();
	$(".home .menu > ul > li").each(function() {
		var me = $(this);
		me.children('a').prepend('<span class="before"></span>')
		homesubmenupaddinit( $(this) );
	});
	$(".home .menu > ul > li").hover(function() {
		$(this).children('ul').stop(true, true).fadeIn('400');
		if ($("html").hasClass('ie8')) {
			$(this).find('.before').stop(true, true).animate(
		    { opacity: 1 },
		    { queue: false, duration: '400' }
		  );
		};
	}, function() {
		if ($("html").hasClass('ie8')) {
			$(this).find('.before').stop(true, true).animate(
		    { opacity: 0 },
		    { queue: false, duration: '400' }
		  );
		};
		var this_ul = $(this).children('ul');
		$(this_ul).stop(true, true).fadeOut('400');
		/*$(this_ul).stop(true, true).animate(
		    { opacity: 0 },
		    { queue: false, duration: '400',
         complete: function(){
              		if (!($(this_ul).hasClass('paddinged'))) {
              			$(this_ul).stop(true, true).hide(1, function(){
	                        $(this_ul).css('padding-left', "");
	                    }); 	
              		} else {
              			$(this_ul).css('display', 'none');
                       
         }
              		}
        }
		  );*/


		/*$(this).children('a').find('.before').animate(
		    { opacity: 0 },
		    { queue: false, duration: '400' }
		  );*/
		
	});
	if ($(".error-page").is(':visible')) {
		$(".bluebox").css('height', '101px');
	};
});
$(window).resize(function(){

	waitForFinalEvent(function(){
      if ($(".bluebox .menu > ul > li.current-menu-parent").length > 0){
		submenupaddresize( $(".bluebox .menu > ul > li.current-menu-parent") );
	  }
	  $(".home .menu > ul > li").each(function() {
		$(this).find('.sub-menu').removeClass('paddinged');
		$(this).find('.sub-menu').css({
				'display': 'block',
				'opacity': '0'
			});
		homesubmenupaddinit( $(this) );
	});
    }, 500, "paddington");
});