<?php
/**
 * The footer for theme.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */
?>

<div class="footer clearfix">
	<div class="big-box clearfix">
	<div class="left clearfix">
		
 <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/w_15.png" alt="" width="26" height="23" /></a>
 <p><?php _e('&copy; 2014 Word to the Wise', 'WttW'); ?></p>
	<?php wp_nav_menu(array("theme_location" => 'footer', 'container' => false, 'items_wrap' => '<ul>%3$s</ul>')); ?>
	<p class="follow"><a href="http://twitter.com/wise_laura" title="Follow me on twitter"><i class="trigger icon-twitter icon-2x"></i></a></p>
	</div>

	<a href="<?php echo get_option('to_footer_button') ?>" class="right"><span><?php _e('client login', 'WttW'); ?></span></a>

</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>