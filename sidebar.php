<?php
$aPresetWidgets = array(
	'blog_widget_area' => array(''),
);

if(isset($_GET['activated']))
{

	update_option('sidebars_widgets', $aPresetWidgets);
}
?>

<?php if(is_sidebar_active('blog_widget_area')) : ?>

	<div class="blog_sidebar">

	<?php dynamic_sidebar('blog_widget_area'); ?>

	</div>


<?php endif; ?>
