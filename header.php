<?php
/**
 * The Header for theme.
 *
 * Displays all of the <head> section and page header
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html <?php language_attributes(); ?> class="no-js ie ie6 lte9 lte8 lte7">  <![endif]-->
<!--[if IE 7]>    <html <?php language_attributes(); ?> class="no-js ie ie7 lte9 lte8 lte7">  <![endif]-->
<!--[if IE 8]>    <html <?php language_attributes(); ?> class="no-js ie ie8 lte9 lte8">       <![endif]-->
<!--[if IE 9]>    <html <?php language_attributes(); ?> class="no-js ie ie9 lte9">            <![endif]-->
<!--[if gt IE 9]> <html <?php language_attributes(); ?> class="no-js ie ie10 lte10">          <![endif]-->
<!--[if !IE]><!--><html <?php language_attributes(); ?>> 	             	       				<!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	
	<?php wp_enqueue_style('component', get_bloginfo('template_directory')."/css/component.css", null, '1.0', 'all'); ?>
	<?php wp_enqueue_style('fonts', get_bloginfo('template_directory')."/fonts/stylesheet.css", null, '1.0', 'all'); ?>
	<?php wp_enqueue_style('style', get_bloginfo('stylesheet_url'), null, '1.0', 'all'); ?>

	<?php
	if(is_singular() && get_option('thread_comments'))
		wp_enqueue_script('comment-reply');
	?>
	<?php wp_enqueue_script('modernizr', get_bloginfo('template_directory')."/js/modernizr.custom.js", null, '1.0', true); ?>
        <?php wp_enqueue_script("zeroclipboard", get_bloginfo('template_directory')."/js/ZeroClipboard.min.js", null, '1.0', 'all'); ?>
	<?php wp_enqueue_script('dlmenu', get_bloginfo('template_directory')."/js/jquery.dlmenu.min.js", array('jquery'), '1.0', true); ?>
	<?php wp_enqueue_script('bxslider', get_bloginfo('template_directory')."/js/jquery.bxslider.min.js", array('jquery'), '1.0', true); ?>
	<!--[if IE 8]>
	 <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/nwmatcher-1.2.5.js"></script>
	 <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/selectivizr-min.js"></script>
	 <![endif]-->
	<?php wp_enqueue_script('functions', get_bloginfo('template_directory')."/js/functions.min.js", array('jquery'), '1.0', true); ?>
	<script type="text/javascript">var switchTo5x=false;</script>
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon_152.png">
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon_120.png">
  <link rel="icon" sizes="48x48" href="/favicon_48.png">	
	<?php wp_head(); ?>
	
</head>
	<?php 
		$post = get_post(get_the_ID()); 
		$slug = $post->post_name;
		$pageTitle = $slug; 
	?>
	<body class="<?php if(is_front_page()) echo custom_body_class(); ?>" >

		<div class="big-box">

			<?php if(is_front_page()) : ?>

			<div class="header subpage_header clearfix">
    <a class="logo-top" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/wttw.png" width="136" height="47" alt="Word to the Wise" /></a>
				
				<div class="qc">
					<span style="color: #fc615e; font-size: 16px; font-family: 'open_sansregular';"><?php _e('Having an email crisis?','WttW'); ?></span> <a href="mailto:<?php echo date('Md') ?>@contact.wordtothewise.com"><?php _e('email us','WttW'); ?></a> <?php _e('or','WttW'); ?> <span style="color: #4551a3; font-size: 14px; font-family: 'open_sansregular';"><a href="tel:+18008239674">800 823-9674</a></span>
				</div>

			</div>

		<?php else : ?>
		<div class="relcont">
		<div class="blueright"></div>
		<div class="submenubg"></div>
		<div class="header subpage_header clearfix">
			
			<div class="othrer">
				
    <a class="logo-top" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/bigw_03.png" alt="mnm" width="59" height="49" /></a>

			</div>

			<div class="bluebox">
				
				<div class="menu clearfix">
					<?php wp_nav_menu(array("theme_location" => 'primary', 'container' => false, 'items_wrap' => '<ul>%3$s</ul>')); ?>
				</div>

				<div id="dl-menu" class="dl-menuwrapper">
      <button class="dl-trigger">Open Menu</button>
					<?php wp_nav_menu(array("theme_location" => 'mobile', 'container' => false, 'items_wrap' => '<ul class="dl-menu">%3$s</ul>')); ?>
				</div>
				<div class="qc2">
    	<span style="font-size: 16px;"><?php _e('Having an email crisis?','WttW'); ?></span> <a href="mailto:<?php echo date('Md') ?>@contact.wordtothewise.com"><?php _e('email us','WttW'); ?></a> <?php _e('or','WttW'); ?> <span style="color: #fff;"><a href="tel:+18008239674">800 823-9674</a></span>
    </div>

			</div>

		</div>
		</div>

		<?php endif; ?>

