<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */
?>


<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( '1 comment', '%1$s comments', get_comments_number(), 'WttW' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h2>

		<ol class="commentlist">
			<?php
				wp_list_comments( array(
					'style'			=>	'ol',
					'callback'      =>  'wttw_comment'
				) );
			?>
		</ol><!-- .comment-list -->

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.' , 'WttW' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php
		$comments_args = array(
		
				'title_reply'			=>	__( 'Comment: ' ), 
				'comment_notes_after'	=>	'',
				'label_submit'			=>	__( 'Post' ),
		
		);
		
		comment_form($comments_args);
	?>

</div>