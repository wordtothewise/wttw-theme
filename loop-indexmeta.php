<?php
/**
 * Loop for home page.
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */
?>
<?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
                <?php if(!is_home() || !in_category('Asides')) { ?>
                <div class="post">
                        <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <div class="single-meta">
                                <span><a href="<?php echo get_month_link(get_the_date("Y"), get_the_date("m")); ?>" class="archive-link"><?php echo get_the_date(); ?></a> by <?php the_author_posts_link(); ?> in <?php the_category(" , "); ?></span>
                        </div>
                        <?php if(is_single() || is_home()) {
                                the_content();
                        } else {
                                the_excerpt();
                        } ?>
                        <?php wttw_tags_and_share(); ?>
                        <span class="loop-comments-link"><?php comments_popup_link(__('No Comments', 'WttW'), __('1 Comment', 'WttW'), __('% Comments', 'WttW'), "comment-link"); ?></a></span>
                </div>
                <?php } ?>
        <?php endwhile; ?>
<?php endif; ?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if ($wp_query->max_num_pages > 1) : ?>
        <div id="nav-below" class="navigation">
                <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Older posts', 'WttW')); ?></div>
                <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', 'WttW')); ?></div>
        </div><!-- #nav-below -->
<?php endif; ?>