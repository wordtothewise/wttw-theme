<?php
/**
 * The single post page template.
 *
 * 
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<?php $bg = get_field('main_background'); ?>

<?php if(is_sidebar_active('blog_widget_area')) : ?>
<div class="builder blog single-blog-post clearfix" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
        <div class="builder_insider single-post clearfix">
        <?php $extrahead = get_option('to_single_extra_title');
        if ($extrahead) { ?>
              <h3 class="extrahead"><?php echo $extrahead; ?></h3>  
        <?php } ?>
        <h2><?php the_title(); ?></h2>
        <div class="single-meta">
				<span><a href="<?php echo get_month_link(get_the_date("Y"), get_the_date("m")); ?>" class="archive-link"><?php echo get_the_date(); ?></a> by <?php the_author_posts_link(); ?> in <?php the_category(" , "); ?></span>
		</div>
        <div class="center sec">
            
                <?php the_content(); ?>
                <?php wttw_tags_and_share(); ?>
                <?php comments_template(); ?>
                <div class="prevnext">
                <?php

                $next_post = get_next_post();
                if(!empty($next_post)) {
                    ?><span class="prev"><i class="icon-backward"></i> <a href="<?php echo get_permalink($next_post->ID); ?>"><?php echo $next_post->post_title; ?></a></span><?php
                }

                $prev_post = get_previous_post();
                if(!empty($prev_post)) {
                    ?><span class="next"><a href="<?php echo get_permalink($prev_post->ID); ?>"><?php echo $prev_post->post_title; ?></a> <i class="icon-forward"></i></span><?php
                }
                ?>
                <br style="clear: both">
                </div>
        </div>

        <div class="three">
                
                <?php get_sidebar(); ?>
        
        </div>
        </div>
</div>
<?php else : ?>

<div class="big-box">
	<div class="open" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
		<div class="abacus single-post">
			<h1><?php the_title(); ?></h1>
			<div class="single-meta">
				<span><a href="<?php echo get_month_link(get_the_date("Y"), get_the_date("m")); ?>" class="archive-link"><?php echo get_the_date(); ?></a> by <?php the_author_posts_link(); ?> in <?php the_category(" , "); ?></span>
			</div>
			<?php the_content(); ?>
                <?php wttw_tags_and_share(); ?>
			<?php comments_template(); ?>
		</div>
	</div>
</div>

<?php endif; ?>

<?php get_footer(); ?>