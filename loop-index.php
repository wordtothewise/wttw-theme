<?php
/**
 * Loop for home page.
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */
?>
<?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
                <div class="post">
                        <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php if(is_single()) : ?>
                                <?php the_content(); ?>
                        <?php else : ?>
                                <?php the_excerpt(); ?>
                        <?php endif; ?>
                        <?php wttw_tags_and_share(); ?>
                </div>
        <?php endwhile; ?>
<?php endif; ?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if ($wp_query->max_num_pages > 1) : ?>
        <div id="nav-below" class="navigation">
                <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Older posts', 'WttW')); ?></div>
                <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', 'WttW')); ?></div>
        </div><!-- #nav-below -->
<?php endif; ?>