<?php
/**
 * Template Name: Team Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<?php $bg = get_field('main_background'); ?>

<div class="management clearfix" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
	
	<div class="info">
		
		<h1><?php the_title(); ?></h1>

		<?php $args = array( 
	   'post_type' => 'person', 
	   'order' => 'DESC',
	   'orderby' => 'date',
	   'posts_per_page' => -1
			);
   query_posts( $args ); ?> 

   <?php while (have_posts()) : the_post(); ?>

   	<div class="atkins clearfix">
   		
   		<?php the_post_thumbnail('person-feature'); ?>

   		<div class="atkins-text">

   			<h2><?php the_title(); ?></h2>
   			
   			<?php the_content(); ?>

   		</div>

   	</div>

   <?php endwhile; ?>

			<?php wp_reset_query(); ?>

	</div>

</div>

<?php get_footer(); ?>