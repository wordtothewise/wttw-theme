<?php
/**
 * Template Name: Home Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); 
$backgrpostop = get_field('home_image_horizontal_position');
$backgrposleft = get_field('home_image_vertical_position');
?>

<div class="banner clearfix" >
	<div class="headerbackground" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>'); <?php if(!empty($backgrpostop) && !empty($backgrposleft)){ echo 'background-position: '.$backgrposleft.' '.$backgrpostop; } ?>">
	<div id="dl-menu" class="dl-menuwrapper">
   <button class="dl-trigger">Open Menu</button>

			<?php wp_nav_menu(array("theme_location" => 'mobile', 'container' => false, 'items_wrap' => '<ul class="dl-menu">%3$s</ul>')); ?>
		
	</div>
	<div class="home-head-inside-wrapper clearfix">
	<div class="text">
		
		<?php the_content(); ?>

		<div class="menu clearfix">
				<?php wp_nav_menu(array("theme_location" => 'primary', 'container' => false, 'items_wrap' => '<ul>%3$s</ul>')); ?>
			
		</div>

	</div>

	
	</div>
	<div class="submenu-bar">
	</div>
	</div>
</div>

<div class="articles">
		<?php dynamic_sidebar('home_widget_area'); ?>
</div>

<?php get_footer(); ?>