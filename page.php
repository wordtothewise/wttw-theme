<?php
/**
 * The static page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<?php $bg = get_field('main_background'); ?>

<div class="big-box">
	<div class="open" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
		<div class="abacus">
			<?php $extrahead = get_field('extra_page_title'); 
        if ($extrahead) { ?>
              <h3 class="extrahead"><?php echo $extrahead; ?></h3>  
        <?php } ?>
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
			<?php the_tags('<span class="tags">',' • ','</span>'); ?>

			<?php $bottomInfo = get_field('bottom_info_text'); ?>

		<?php if(!empty($bottomInfo)) : ?>

		<div class="bottom-info">

		<h4><?php the_field('bottom_info_text'); ?></h4>
		<?php the_field('bottom_info_link'); ?>

                </div>

		<?php endif; ?>

			
		</div>
	</div>
</div>


<?php get_footer(); ?>