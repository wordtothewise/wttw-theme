<?php
/**
 * The 404 page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); ?>
<?php if(have_posts()) the_post (); ?>

        <div class="error-page">

        	<p style="font-size: 25px; margin-top: 40px;">
                        <?php _e("We're sorry, but the page you are looking for cannot be found. What
                        should you do at this point? Here are some options:", "WttW"); ?>
                </p>
                <ul>
                        <li><?php _e("If you typed in a URL, check that it is typed in correctly.", "WttW"); ?></li>
                        <li><?php _e("Perhaps it was just a fluke, and if you try again by clicking refresh, it'll pop right up!", "WttW"); ?></li>
                        <li><?php _e("Or head back to our home page", "WttW"); ?> <a href="<?php bloginfo("url"); ?>"><?php bloginfo("url"); ?></a> <?php _e("and navigate from there.", "WttW"); ?> </li>
                </ul>

								</div>

<?php comments_template( '', true ); ?>

<?php get_footer(); ?>
