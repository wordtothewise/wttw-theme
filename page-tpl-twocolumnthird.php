<?php
/**
 * Template Name: Two Column Third Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage WttW
 * @since WttW 1.0
 */

get_header(); the_post(); ?>

<?php $bg = get_field('main_background'); ?>

<div class="builder clearfix" <?php if(!empty($bg)) : ?>style="background: url('<?php echo $bg; ?>') repeat-x;"<?php endif; ?>>
	<div class="builder_insider">
	<h2><?php the_title(); ?></h2>

	<div class="center">
		
		<?php the_content(); ?>

<?php $bottomInfo = get_field('bottom_info_text'); ?>

		<?php if(!empty($bottomInfo)) : ?>

		<div class="bottom-info">

		<h4><?php the_field('bottom_info_text'); ?></h4>
		<?php the_field('bottom_info_link'); ?>

                </div>

		<?php endif; ?>

	</div>

	<?php $help = get_field('thtpl_text'); ?>

	<?php if(!empty($help)) : ?>
	
	<div class="help thirdpagetpl">
 	<?php the_field('thtpl_text'); ?>
 	<?php 
 	$helpurl = get_field('thtpl_url');
 	$helplink = get_field('thtpl_link');
 	if (!empty($helpurl) && !empty($helplink)) : ?>
 		<a href="<?php the_field('thtpl_url'); ?>"><?php the_field('thtpl_link'); ?><img src="<?php bloginfo('template_directory'); ?>/images/read-more.png" alt="icon" /></a>
 	<?php endif; ?>

	</div>

	<?php endif; ?>
</div>
</div>

<?php get_footer(); ?>